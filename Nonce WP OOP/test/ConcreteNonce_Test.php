<?php


require_once ('../vendor/PHPUnit/PHPUnit/src/Framework/TestCase.php');
require_once ('../vendor/autoload.php');
require_once ("NonceWPOOPMockupMethods.php");
/**
 * ConcreteNonce test case.
 */
class ConcreteNonce_Test extends TestCase
{

    /** test nonce value **/
    public function testGenerate_nonceValue()
    {
        $nonce = new nonceoop\ConcreteNonce();
        $valNonce = $nonce->generateNonce();
        $nonce->setNonceValue($valNonce);
        $this->assertEquals($valNonce, $nonce->getNonceValue());
    }
    
    
    /** test nonce name **/
    public function testGenerate_nonceName()
    {
        $nonce = new nonceoop\ConcreteNonce();
        $nonce->setNonceName("name1");
        $this->assertEquals("name1", $nonce->getNonceName());
    }
    
    /** test nonce action **/
    public function testGenerate_nonceAction()
    {
        $nonce = new nonceoop\ConcreteNonce();
        $nonce->setNonceAction("Action1");
        $this->assertEquals("Action1", $nonce->getNonceAction());
    }
    
    /** test nonce wrong value **/
    public function testGenerate_nonceValueWrong()
    {
        $nonce = new nonceoop\ConcreteNonce();
        $valNonce = $nonce->generateNonce();
        $nonce->setNonceValue($valNonce);
        $this->assertNotEquals(nonceoop\NonceOOPConstants::EMPTY_STRING, $nonce->getNonceValue());
    }
    
}

