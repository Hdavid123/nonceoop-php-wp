<?php


use nonceoop\NonceOOPConstants;

require_once ('../vendor/PHPUnit/PHPUnit/src/Framework/TestCase.php');
require_once ('../vendor/autoload.php');
require_once ("NonceWPOOPMockupMethods.php");
/**
 * FieldNonce test case.
 */
class FieldNonceTest extends TestCase
{

    /**  test Field nonce value **/
    public function testGenerate_nonceValue()
    {
        $url = "http://www.test.com/page=2";
        $nonce = new nonceoop\FieldNonce();
        $valNonce = $nonce->generateFieldNonce(true,true);
        $this->assertNotNull($nonce->getNonceValue());
    }
    
    /** test Field nonce name **/
    public function testGenerate_nonceName()
    {
        $nonce = new nonceoop\FieldNonce();
        $nonce->setNonceName("name1");
        $this->assertEquals("name1", $nonce->getNonceName());
    }
    
    /** test Field nonce action **/
    public function testGenerate_nonceAction()
    {
        $nonce = new nonceoop\FieldNonce();
        $nonce->setNonceAction("Action1");
        $this->assertEquals("Action1", $nonce->getNonceAction());
    }
    
    /** test Field nonce wrong value  **/
    public function testGenerate_nonceValueWrong()
    {
        $nonce = new nonceoop\FieldNonce();
        $valNonce = $nonce->generateNonce();
        $nonce->setNonceValue($valNonce);
        $this->assertNotEquals(nonceoop\NonceOOPConstants::EMPTY_STRING, $nonce->getNonceValue());
    }
    
}

