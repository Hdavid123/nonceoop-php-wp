<?php


use nonceoop\NonceOOPConstants;

require_once ('../vendor/PHPUnit/PHPUnit/src/Framework/TestCase.php');
require_once ('../vendor/autoload.php');
require_once ("NonceWPOOPMockupMethods.php");
/**
 * URLNonce test case.
 * Only URLNonce
 */
class URLNonceTest extends TestCase
{

    /** test URL nonce value **/
    public function testGenerate_nonceValue()
    {
        $url = "http://www.test.com/page=2";
        $nonce = new nonceoop\UrlNonce();
        $valNonce = $nonce->generateUrlNonce($url);
        $this->assertNotNull($nonce->getNonceValue());
    }
    
    /** test URL nonce  name **/
    public function testGenerate_nonceName()
    {
        $nonce = new nonceoop\UrlNonce();
        $nonce->setNonceName("name1");
        $this->assertEquals("name1", $nonce->getNonceName());
    }
    
    /** test URL nonce Action **/
    public function testGenerate_nonceAction()
    {
        $nonce = new nonceoop\UrlNonce();
        $nonce->setNonceAction("Action1");
        $this->assertEquals("Action1", $nonce->getNonceAction());
    }
    
    /** test wrong url nonce value **/
    public function testGenerate_nonceValueWrong()
    {
        $nonce = new nonceoop\UrlNonce();
        $valNonce = $nonce->generateNonce();
        $nonce->setNonceValue($valNonce);
        $this->assertNotEquals(nonceoop\NonceOOPConstants::EMPTY_STRING, $nonce->getNonceValue());
    }
    
}

