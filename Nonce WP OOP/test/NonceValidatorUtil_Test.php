<?php


require_once ('../vendor/PHPUnit/PHPUnit/src/Framework/TestCase.php');
require_once ('../vendor/autoload.php');
require_once ("NonceWPOOPMockupMethods.php");
/**
 * ConcreteNonce test case.
 */
class NonceValidatorDIUtil_Test extends TestCase
{

    /** test polymorphysm nonce value **/
    public function testExecuteNonceValidation_nonceValue()
    {
        $nonce = new nonceoop\NonceValidatorUtil();
        $this->assertTrue($nonce->executeNonceValidation($nonce->getNonceValue()));
    }
    

    
}

