<?php


require_once ('../vendor/PHPUnit/PHPUnit/src/Framework/TestCase.php');
require_once ('../vendor/autoload.php');
require_once ("NonceWPOOPMockupMethods.php");
/**
 * ConcreteNonce test case.
 */
class NonceValidatorDIUtil_Test extends TestCase
{

    /** IMPORTANT IMPORTANT !! THIS METHOD WILL FAIL BECAUSE PHP UNIT DOESN'T SUPPORT $_REQUEST ***/
    /** test Util Class to validate nonce **/
    public function testExecuteNonceValidation_nonceValue()
    {
        $nonce = new nonceoop\UrlNonce();
        $nonce->generateUrlNonce("http://www.test.com/");
        $valNonce = $nonce->getNonceValue();
        $noneUtil = new nonceoop\NonceValidatorDIUtil();
        $noneUtil->setNonceObj($nonce);
        $this->assertTrue($noneUtil->executeNonceValidation($valNonce));
    }
    

    
}

