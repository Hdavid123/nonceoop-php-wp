<?php


function wp_create_nonce(){
    return substr(sha1(time()), 0, 16);;
}


function esc_html( $text ) {
    
    return htmlspecialchars($text, ENT_QUOTES);
}

function esc_attr( $text ) {
    
    return htmlspecialchars($text, ENT_QUOTES);
}

function wp_verify_nonce($val, $val2){
    return true;
}

function wp_referer_field( $echo = true ) {
    $requestUri = "URI_SAMPLE";
    $referer_field = '<input type="hidden" name="_wp_http_referer" value="' . esc_attr( stripslashes($requestUri) ) . '" />';
    
    if ( $echo ) {
        echo $referer_field;
    }
    return $referer_field;
}

function add_query_arg($key, $value, $url){
    $data = array(
        $key => $value
    );
    
    echo http_build_query($data);
}
?>

