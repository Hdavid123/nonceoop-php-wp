<?php

namespace nonceoop;

class NonceOOPConstants {

    public const DEFFAULT_NONCE_NAME = "_wpnonce";

    public const APLPHA_NUMERIC_PATTERN_REGEX = "/[^a-zA-Z0-9]+/";

    public const  EMPTY_STRING = "";

    public const  MINUS_ONE = -1;

}

?>