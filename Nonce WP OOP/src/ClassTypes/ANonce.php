<?php

namespace nonceoop;

/**
 * @author HDAOUD
 * 
 * Implementation class for Nonce Interface
 *
 */
abstract class ANonce implements INonce {
    
    /**
     * Action For Nonce
     */
    private $nonceAction;
    
    /**
     * NAme for Nonce
     */
    private  $nonceName;
    
    /**
     * 
     * Value for Nonce(AlphaNumeric)
     */
    private $nonceValue;
    
    
    /**
     * Constructor With deffaulting Attributes to @see {@link nonceOOP
     * @param string $nonceAction
     * @param string $nonceName
     */
    public function __construct( $nonceAction = NonceOOPConstants::MINUS_ONE, $nonceName = NonceOOPConstants::DEFFAULT_NONCE_NAME ) {
        $this->setNonceAction($nonceAction);
        $this->setNonceName($nonceName);
        $this->setNonceValue(null);
    }
    

    public function getNonceValue()
    {
        return $this->nonceValue;
    }
    
    public function setNonceName($whatNonceName)
    {
        $this->nonceName = $whatNonceName;
    }
    
    public function getNonceAction()
    {
        return $this->nonceAction;
    }
    
    public function setNonceValue($whatNonceValue)
    {
        $this->nonceValue = $whatNonceValue;
    }
    
    public function setNonceAction($whatNonceAction)
    {
        $this->nonceAction = $whatNonceAction;
    }
    
    public function getNonceName()
    {
        return $this->nonceName;
    }
    

}
?>