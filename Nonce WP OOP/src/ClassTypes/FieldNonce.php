<?php 

namespace nonceoop;

final class FieldNonce extends ConcreteNonce{
    
    public function __construct( $nonceAction = NonceOOPConstants::MINUS_ONE, $nonceName = NonceOOPConstants::DEFFAULT_NONCE_NAME ) {
        parent::__construct( $nonceAction, $nonceName );
    }
    
    
    public function generateFieldNonce($referer = true, $echoFieldObj= true) {
        $this->setNonceValue($this->generateNonce());
        $name = esc_attr( $this->getNonceName() );
        $nonce_field = '<input type="hidden" id="' . $name . '" name="' . $name . '" value="' . $this->getNonceValue() . '" />';
        if ($referer) {
            $nonce_field .= wp_referer_field( false );
        }
        if ($echoFieldObj) 
            echo $nonce_field;
        return $nonce_field;
    }
}

?>