<?php

namespace nonceoop;

/**
 * @author HDAOUD
 * 
 * Interface to achive 
 *
 */
interface INonce {
       
    /**
     * Nonce Action Attibute
     */
    public function getNonceAction();
    
    
    /**
     * set Nonce Action
     * 
     * @param $whatNonceAction
     */
    public function  setNonceAction($whatNonceAction);
    
    
    /**
     * Method to return Nonce Name
     * 
     */
    public function getNonceName();
    
    
    /**
     * Method to set Nonce Name
     * 
     * @param $whatNonceName
     */
    public function setNonceName($whatNonceName);
    
    
    
    /**
     * Method to get Nonce Value
     * 
     */
    public function getNonceValue();
    
    
    
    /**
     * Method to set Nonce Value
     * 
     * @param $whatNonceValue
     */
    public function setNonceValue($whatNonceValue);

    
    
}
?>