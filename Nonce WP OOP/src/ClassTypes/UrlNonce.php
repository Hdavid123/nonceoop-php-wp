<?php
namespace nonceoop;

final class UrlNonce extends ConcreteNonce{
    
    public function __construct( $nonceAction = NonceOOPConstants::MINUS_ONE, $nonceName = NonceOOPConstants::DEFFAULT_NONCE_NAME ) {
        parent::__construct( $nonceAction, $nonceName );
    }
      /** Method is used to generate ecplicitly URL Nonce and return the created URL **/     
    function generateUrlNonce($actionUrl) {
        $this->setNonceValue($this->generateNonce());
        return esc_html( add_query_arg( $this->getNonceName(), $this->getNonceValue(), NonceGenericUtil::formatAndInUrl($actionUrl)));
    }
    
}
    
?>