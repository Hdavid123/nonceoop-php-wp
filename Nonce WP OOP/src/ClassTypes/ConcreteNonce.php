<?php
namespace nonceoop;

class ConcreteNonce extends ANonce{
    
    public function __construct( $nonceAction = NonceOOPConstants::MINUS_ONE, $nonceName = NonceOOPConstants::DEFFAULT_NONCE_NAME ) {
        parent::__construct( $nonceAction, $nonceName );
    }

    
    /** **/
    public function generateNonce() {
        $this->setNonceValue(wp_create_nonce($this->getNonceAction()));
        return $this->getNonceValue();
    }
}


?>