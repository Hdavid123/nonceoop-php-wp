<?php

namespace nonceoop;

/**
 * This asbtract class is a sample to show Depedency Injection
 * 
 * it will be using:
 *      1- Hard DI by constructor
 *      2- Soft DI by a Magic Setter Method 
 *      3- Setter for the Object
 * 
 * @author HDAOUD
 *
 */
 class NonceValidatorDIUtil {
    
    private $nonceObj; 
    
    /** Constructor to create object utils**/ 
    public function __construct() {
       
    }
    /** magic set method to set any created nonce object **/
    public function __get($name) {
        return $this->$name;
    }
    
    /** magic get method to get any created nonce object **/
    public function __set($name, $value) {
        return $this->$name = $value;
    }
    
    /** this method is used to validat URL Nonce **/
    private function validateUrlNonce() {
        if ( isset($_REQUEST[$this->nonceObj->getNonceName()])) {
            $this->nonceObj->setNonceValue(NonceGenericUtil::getAlphaNumericValuesOnly($_REQUEST[$this->nonceObj->getNonceName()]));
            return $this->validateNonce();
        }
        return false;
    } 
    
    /** Method to validate Url Nonces and non Url Nonces based on Object Type **/
    public function executeNonceValidation($param_nonceValue) {
        if ($this->nonceObj instanceof UrlNonce) {
            $this->validateUrlNonce();
        }else{
            $this->nonceObj->setNonceValue($param_nonceValue);
            return $this->validateNonce();
        }
    }
    
    private function validateNonce(){
        return wp_verify_nonce($this->nonceObj->getNonceValue(), $this->nonceObj->getNonceAction());
    }
    /**
     * @return mixed
     */
    public function getNonceObj()
    {
        return $this->nonceObj;
    }

    /**
     * @param mixed $nonceObj
     */
    public function setNonceObj($nonceObj)
    {
        $this->nonceObj = $nonceObj;
    }

    
    
}

?>