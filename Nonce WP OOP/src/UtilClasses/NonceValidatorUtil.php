<?php 

namespace nonceoop;

/**
 * @author hdaoud
 *
 *  class is used as an extensiation of ANonce
 *  
 *  this class will work as polymorphysm and do the validation at the same time 
 *  
 *  it doesn't need to declare another instance of util class or use any other singlton to do validatio 
 *  
 */
final class NonceValidatorUtil extends ANonce {

    
   /** constructor will call parent class contructor which is a ANonce **/ 
    public function __construct( $nonceAction = NonceOOPConstants::MINUS_ONE, $nonceName = NonceOOPConstants::DEFFAULT_NONCE_NAME ) {
        parent::__construct( $nonceAction, $nonceName );
    }

    /** Method to be used to validate nonce for any nonce object within Nonce OOP Scope **/
    public function executeNonceValidation($param_nonceValue) {
        if ($this instanceof UrlNonce) {
            $this->validateUrlNonce();
        }else{
            $this->setNonceValue($param_nonceValue);
            return $this->validateNonce();
        }
    }

    /** will be used as primary method if initiated object is type not a URLNonce**/
    private function validateNonce(){
        return wp_verify_nonce($this->getNonceValue(), $this->getNonceAction());
    }
    
    /** will be used as primary method if initiated object is type of URLNonce**/
    private function validateUrlNonce() {
        if (isset($_REQUEST[$this->getNonceName()])) {
            $this->setNonceValue(NonceGenericUtil::getAlphaNumericValuesOnly($_REQUEST[$this->getNonceName()]));
            return $this->validateNonce();
        }
        return false;
    }


}

?>