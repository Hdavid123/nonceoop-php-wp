<?php

namespace nonceoop;

/**
 * This is Generic Util class 
 * 
 * This class has a singleton Pattern in order to achieve common Functionalities accross the Appliocation 
 * 
 * @author HDAOUD
 *
 */
final class NonceGenericUtil{
    
    /** private Constructor to make it singleton **/
    private function __construct(){
        
    }
    /**
     *  
     *  Method is used to espcape non Alpha-Numeric Chars from String Using preg_replace
     * 
     *      public static method to access singleton
     *      
     * @param  $whatString
     * @return mixed **/
    public static function getAlphaNumericValuesOnly($whatString){
        return preg_replace(APLPHA_NUMERIC_PATTERN_REGEX, EMPTY_STRING, $whatString);
    }
    
    
    /**
     *
     */
     public static function formatAndInUrl($actionurl)
     {
         return str_replace('&amp;', '&', $actionurl);
    }
}

?>