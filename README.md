** WORDPRESS NONCES MANAGMENT USING PHP OOP**


- Usage:

1- import all classes as they are  by hierarchy

2- in your composer make sure you point autload to this classesc folders

3- Access Nonce functionality by namespace: nonceoop

from any class you want you can start creating nonces by the below:

 $nonce = new nonceoop\UrlNonce();
 $nonce = new nonceoop\FieldNonce();
 $nonce = new nonceoop\ConcreteNonce();
 
To validate Nonces you can use util Class:
	NonceValidatorUtil
	
To create Nonce With Validation by using polymorphism:
	$noneUtil = new nonceoop\NonceValidatorDIUtil();
	Note: this class is an extensiation of ANonce that will have the capability of validation



TECHNICAL EXPLANATION:

Nonce creation in wordpress is dependant on the object that you try to create and it could be in 3 forms:

1. Nonce object. 
2. Field Nonce.
3. URL Nonce

All these nonces share same attributes:

1- Name
2- Action
3- Value

All these nonces will end up using the same method to create the nonce which will be 

use wp_create_nonce( string|int $action = -1 );

first of all we will create an interface called INonce that will hold same attributes from different sources:

    public function getNonceAction();
    
    public function  setNonceAction($whatNonceAction);
    
    public function getNonceName();
    
    public function setNonceName($whatNonceName);
    
    public function getNonceValue();
    
    public function setNonceValue($whatNonceValue);


Now we will create an abstract class called ANonce to implement all INonce Methods and will be using a constructor to populate this attributes

We will startup with a ConcreteNonce that will share the same generateNonce() to generate nonce values for all subsclass an same class;

This new ConcreteNonce class will be used to extend 2 types: 

1- FieldNonce
2- URLNonce

In FieldNonce we will create a generateFieldNonce
Which will be generating a field nonce specifically as it's not a method for other nonces

In URLNonce we will create a generateUrlNonce
Which will be generating a URL nonce specifically as it's not a method for other nonces

In addition we will create 2 classes for validation:
1- NonceValidatorDIUtil
2- NonceValidatorUtil

NonceValidatorDIUtil: is a util class used to validate nonce based on type by using DI as part of the design
NonceValidatorUtil: is an extensiation of ANonce to validate nonce based on type by using polymorphism